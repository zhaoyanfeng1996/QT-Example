# QT-Example

#### 介绍
QT示例代码

- baidu_speech：调用百度语音REST API进行语音识别和语音合成。
- chatbot：调用竹间智能机器人REST API进行对话。
- i18n：通过菜单切换界面语言。
- mqtt_client：MQTT客户端。
- speechbot：语音对话机器人，综合baidu_speech和chatbot的内容。
- testface：调用百度人脸识别REST API，进行人脸识别，需要使用opencv，目前只支持Linux编译。