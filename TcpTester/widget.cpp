#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    sock = new QTcpSocket(this);
    connect(sock, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(sock, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(sock, SIGNAL(readyRead()), this, SLOT(onReceived()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::onConnected()
{
    ui->textBrowser->append(QString("%1 is connected").arg(ui->ipEdit->text()));
    ui->connectButton->setText("Disconnect");
}

void Widget::onDisconnected()
{
    ui->textBrowser->append(QString("%1 is disconnected").arg(ui->ipEdit->text()));
    ui->connectButton->setText("Connect");
}

void Widget::onReceived()
{
    QByteArray msg = sock->readAll();
    ui->textBrowser->append("> " + QString(msg).simplified());
}

void Widget::on_connectButton_clicked()
{
    QString host = ui->ipEdit->text();
    quint16 port = static_cast<quint16>(ui->portBox->value());
    if (sock->state() == QAbstractSocket::UnconnectedState)
    {
        sock->connectToHost(host, port);
    }
    else {
        sock->disconnectFromHost();
    }
}

void Widget::on_sendButton_clicked()
{
    QString msg = ui->msgEdit->text();
    msg += "\r\n";
    sock->write(msg.toUtf8());
    ui->textBrowser->append("< " + msg.simplified());
}
