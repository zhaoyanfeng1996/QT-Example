﻿#include "widget.h"
#include <QCameraInfo>
#include <QCameraViewfinderSettings>
#include <QMouseEvent>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    //窗口置顶 无边框
    this->setWindowFlags(Qt::WindowStaysOnTopHint|Qt::FramelessWindowHint);
    camera = new QCamera(QCameraInfo::defaultCamera(), this);
    viewfinder = new QCameraViewfinder(this);
    camera->setViewfinder(viewfinder);

    QCameraViewfinderSettings settings;
    /* available resolutions
     * 640 360
     * 640 480
     * 1280 720
     * 1280 960
     */
    settings.setResolution(640, 480);
    viewfinder->resize(320, 240);
    camera->setViewfinderSettings(settings);

    camera->start();
}

void Widget::mousePressEvent(QMouseEvent *event)
{
    _pos = event->globalPos() - this->pos();
}

void Widget::mouseMoveEvent(QMouseEvent *event)
{
    move(event->globalPos() - _pos);
}

void Widget::mouseReleaseEvent(QMouseEvent *event)
{
    move(event->globalPos() - _pos);
}

Widget::~Widget()
{
    camera->stop();
}

